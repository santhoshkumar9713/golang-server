package main

import (
	"golang-server/database"
	"golang-server/models"
	"golang-server/urls"

	"github.com/gin-gonic/gin"
)

func main() {
	database.InitRedis()
	db := database.ConnectDB()
	// Auto Migrate the schema
	db.AutoMigrate(&models.User{}, &models.Provider{})
	router := gin.Default()
	urls.Router(router)
	// var user []models.User
	// var provider models.Provider
	// db.Joins("User").Where("user_id = ?", 1).First(&provider)
	// fmt.Println(provider.UserID, provider.Name, provider.User)
	// result := map[string]interface{}{}
	// db.Model(&User{}).Select("users.name, emails.email").Joins("left join emails on emails.user_id = users.id").Scan(&result{})

	// db.Model(&models.User{}).Select("users.id, users.age, providers.name").Joins("left join providers on providers.user_id = users.id").Scan(&result)
	// fmt.Println(result)
	// for _, i := range user {
	// 	fmt.Println(i)
	// }
	router.Run(":3000")
}
