package cache_test

import (
	"context"
	"fmt"
	"golang-server/cache"
)

var ctx context.Context

func cacheSetTest() {
	redisCache := cache.New()
	cacheValue := &struct {
		Hello string
	}{
		Hello: "world",
	}
	redisCache.Set(ctx, "123", cacheValue, &cache.Options{})
}

func cacheGetTest() {
	redisCache := cache.New()
	res := redisCache.Get(ctx, "123")
	fmt.Println("result", res)
}

func CacheDeleteTest() {
	redisCache := cache.New()
	redisCache.Delete(ctx, "123")
	res1 := redisCache.Get(ctx, "123")
	fmt.Println("result", res1)
}
