package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"golang-server/database"

	"github.com/go-redis/redis/v8"
)

type Cache struct {
	client *redis.Client
}

func New() *Cache {
	return &Cache{
		client: database.RedisClient(),
	}
}

func (c *Cache) Set(ctx context.Context, key, object interface{}, options *Options) {
	cacheKey := getCacheKey(key)
	cachedData, e := json.Marshal(object)
	if e != nil {
		fmt.Println(e)
	}
	err := c.client.Set(ctx, cacheKey, cachedData, options.ExpirationValue()).Err()
	if err != nil {
		fmt.Println("Redis set error", err)
	}
}

func (c *Cache) Get(ctx context.Context, key string) interface{} {
	cacheKey := getCacheKey(key)
	val, err := c.client.Get(ctx, cacheKey).Result()
	if err != nil {
		return nil
	}
	var parsedData interface{}
	json.Unmarshal([]byte(val), &parsedData)
	return parsedData
}

func (c *Cache) Delete(ctx context.Context, key string) {
	err := c.client.Del(ctx, key).Err()
	if err != nil {
		fmt.Println("Delete Cache error", err)
	}
}

func getCacheKey(key interface{}) string {
	switch key.(type) {
	case string:
		return key.(string)
	default:
		return "123"
	}
}
