package schema

import (
	"fmt"
	"golang-server/database"
	"golang-server/models"

	"github.com/graphql-go/graphql"
)

var userType = graphql.NewObject(graphql.ObjectConfig{
	Name: "User",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.Int,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"age": &graphql.Field{
			Type: graphql.Int,
		},
	},
})

type UserView struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func BuildUser(user *models.User) UserView {
	userView := UserView{
		ID:   user.ID,
		Name: user.Name,
		Age:  user.Age,
	}
	return userView
}

func getUserFields() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(userType),
		Description: "Get User",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			if id, ok := p.Args["id"].(int); ok {
				var user models.User
				database.GetDB().Where("id = ?", id).Find(&user)
				view := BuildUser(&user)
				fmt.Println(view)
				return view, nil
			}
			var users []models.User
			var allUsers []UserView
			database.GetDB().Find(&users)
			for _, user := range users {
				allUsers = append(allUsers, BuildUser(&user))
			}
			return allUsers, nil
		},
	}
}
