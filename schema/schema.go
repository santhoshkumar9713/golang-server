package schema

import (
	"fmt"

	"github.com/graphql-go/graphql"
	"github.com/newrelic/go-agent/v3/integrations/nrgraphqlgo"
)

func NewSchema() *graphql.Schema {
	rootQuery := getRootQuery()
	// rootMutation := getRootMutation()
	schemaConfig := graphql.SchemaConfig{
		Query: rootQuery,
		// Mutation:   rootMutation,
		Extensions: []graphql.Extension{nrgraphqlgo.Extension{}},
	}
	schema, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		fmt.Println("Error on creating new schema", err)
	}
	return &schema
}

func getRootQuery() *graphql.Object {
	fields := graphql.Fields{
		"user": getUserFields(),
	}
	rootQuery := graphql.ObjectConfig{
		Name:   "RootQuery",
		Fields: fields,
	}
	return graphql.NewObject(rootQuery)
}

// func getRootMutation() *graphql.Object {
// 	fields := graphql.Fields{
// 		"user": getUserFields(),
// 	}
// 	rootQuery := graphql.ObjectConfig{
// 		Name:   "RootMutation",
// 		Fields: fields,
// 	}
// 	return graphql.NewObject(rootQuery)
// }
