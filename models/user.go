package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID        uint   `gorm:"primaryKey" json:"id"`
	Name      string `gorm:"index" json:"name"`
	Age       int    `json:"age"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Provider struct {
	gorm.Model
	UserID uint   `json:"userId"`
	User   User   `gorm:"foreignkey:UserID;constraint:OnDelete:CASCADE;"`
	Name   string `json:"name"`
}

func (user User) NewProvider(userId uint) Provider {
	return Provider{
		UserID: userId,
		Name:   "GitHub",
	}
}
