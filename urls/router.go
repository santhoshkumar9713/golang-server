package urls

import (
	"golang-server/controllers"
	"golang-server/docs"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// @BasePath /api/v1

// PingExample godoc
// @Summary ping example
// @Schemes
// @Description do ping
// @Tags example
// @Accept json
// @Produce json
// @Success 200 {string} Helloworld
// @Router /example/helloworld [get]
func Helloworld(g *gin.Context) {
	g.JSON(200, "helloworld")
}

func Router(r *gin.Engine) {
	r.GET("/", Helloworld)
	docs.SwaggerInfo.BasePath = "/"
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.GET("/graphql", controllers.Query)
	v1 := r.Group("/api/v1")
	{
		user := v1.Group("user")
		{
			user.GET("/santhosh", controllers.GetAllUsers)
			user.POST("/", controllers.CreateUser)
		}
		provider := v1.Group(("/provider"))
		{
			provider.POST("/", controllers.CreateProvider)
			provider.GET("/", controllers.GetAllProviders)
		}

	}
}
