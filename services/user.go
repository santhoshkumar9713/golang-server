package services

import (
	"fmt"
	"golang-server/cache"
	"golang-server/database"
	"golang-server/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateUserTask(ctx *gin.Context, user *models.User) {
	db := database.GetDB()
	db.Create(&user)
	p := models.Provider{
		Name: "Google",
		User: *user,
	}
	db.Create(&p)
	ctx.JSON(http.StatusOK, &user)
}

func GetAllUsersTask(ctx *gin.Context, users *[]models.User) {
	redisCache := cache.New()
	res := redisCache.Get(ctx, "all_users")
	if res != nil {
		ctx.JSON(200, res)
		return
	}
	db := database.GetDB()
	db.Find(&users)
	redisCache.Set(ctx, "all_users", users, &cache.Options{})
	ctx.JSON(200, users)
}

func GetAllProvidersTask(ctx *gin.Context, providers *[]models.Provider) {
	db := database.GetDB()
	db.Preload("User", "id = ?", 1).Find(&providers)
	for i, provider := range *providers {
		fmt.Println(i, provider.User.Name)
	}
	ctx.JSON(200, providers)
}

func CreateProviderTask(ctx *gin.Context, provider *models.Provider) {
	var user models.User
	db := database.GetDB()
	db.Where("ID = ?", provider.UserID).Find(&user)
	provider.User = user
	db.Create(&provider)
	ctx.JSON(http.StatusOK, &provider)
}
