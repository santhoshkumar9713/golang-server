package database

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

func ConnectDB() *gorm.DB {
	dsn := "host=localhost user=biplocal dbname=gorm port=5432"
	db, err = gorm.Open(postgres.New(postgres.Config{
		DSN:                  dsn,
		PreferSimpleProtocol: true,
	}), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		fmt.Println("Failed connected to db")
	}
	return db
}

func GetDB() *gorm.DB {
	return db
}
