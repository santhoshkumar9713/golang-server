package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"golang-server/cache"
	"golang-server/schema"

	"github.com/gin-gonic/gin"
	"github.com/graphql-go/graphql"
)

func Query(c *gin.Context) {
	query := c.Query("query")
	var variables map[string]interface{}
	if query == "" {
		var body struct {
			Query     string                 `json:"query"`
			Variables map[string]interface{} `json:"variables"`
		}
		if err := json.NewDecoder(c.Request.Body).Decode(&body); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		}
		query = body.Query
		variables = body.Variables
	}
	// Redis cache here which will check the cache first with the query as a key and return the cache data if present.
	// Else we set the query as key and data as redis cache
	redisCache := cache.New()
	var ctx = context.Background()
	res := redisCache.Get(ctx, query)
	if res != nil {
		fmt.Println(res)
		c.JSON(200, res)
		return
	}
	result := executeQuery(c.Request.Context(), query, variables, *schema.NewSchema())
	redisCache.Set(ctx, query, result, &cache.Options{})
	c.JSON(200, result)
}

func executeQuery(context context.Context, query string, variables map[string]interface{}, schema graphql.Schema) *graphql.Result {
	result := graphql.Do(graphql.Params{
		Schema:         schema,
		RequestString:  query,
		Context:        context,
		VariableValues: variables,
	})
	if len(result.Errors) > 0 {
		fmt.Println("Error on Graphql query do", result.Errors)
	}
	return result
}
