package controllers

import (
	"fmt"
	"golang-server/models"
	"golang-server/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateUser(ctx *gin.Context) {
	user := models.User{}
	err := ctx.BindJSON(&user)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	}
	services.CreateUserTask(ctx, &user)
}

func GetAllUsers(ctx *gin.Context) {
	users := []models.User{}
	services.GetAllUsersTask(ctx, &users)
}

func GetAllProviders(ctx *gin.Context) {
	providers := []models.Provider{}
	services.GetAllProvidersTask(ctx, &providers)
}

func CreateProvider(ctx *gin.Context) {
	provider := models.Provider{}
	err := ctx.BindJSON(&provider)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	}
	fmt.Println(provider.UserID, provider.Name)
	services.CreateProviderTask(ctx, &provider)
}
